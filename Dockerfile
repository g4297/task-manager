FROM openjdk:11-jdk-oracle
RUN groupadd -r springgroup && useradd -r springuser -g springgroup
USER springuser:springgroup
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
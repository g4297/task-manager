package ru.company.taskmanager;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.springframework.test.util.AssertionErrors.assertNotEquals;
import static org.springframework.test.util.AssertionErrors.assertTrue;

@SpringBootTest
class TaskManagerApplicationTests {

	@Test
	void contextLoads() {
		assertTrue("Test passed", true);
	}

}

package ru.company.taskmanager.data;

import lombok.Data;
import ru.company.taskmanager.service.entity.TaskDTO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Instant;

@Data
@Entity
@Table(name = "task")
public class Task {

    public Task() {
    }

    public Task(String title, String description, byte priority, Instant startTime) {
        this.title = title;
        this.description = description;
        this.priority = priority;
        this.startTime = startTime;
    }

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)

    private Long id;
    private String title;
    private String description;
    private boolean done;
    private byte priority;
    private Instant startTime;

    public void copyFromDTO(TaskDTO dto) {
        if (dto.getTitle() != null) title = dto.getTitle();
        if (dto.getDescription() != null) description = dto.getDescription();
        if (dto.getDone() != null) done = dto.getDone();
        if (dto.getPriority() != null) priority = dto.getPriority();
        if (dto.getStartTime() != null) startTime = dto.getStartTime();
    }
}

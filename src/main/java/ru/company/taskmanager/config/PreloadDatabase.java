package ru.company.taskmanager.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.company.taskmanager.data.Task;
import ru.company.taskmanager.data.TaskRepository;

import java.time.Instant;

@Configuration
public class PreloadDatabase {

    private static final Logger log = LogManager.getLogger(PreloadDatabase.class);

    @Bean
    CommandLineRunner initDatabase(TaskRepository repository) {

        return args -> {
            log.info("Preloading " + repository.save(new Task("отпуск", "пора ехать", (byte) 3, Instant.parse("2022-08-01T00:00:00Z"))));
            log.info("Preloading " + repository.save(new Task("проект", "сделать проект в Otus", (byte) 1, Instant.now())));
        };
    }
}

package ru.company.taskmanager.service.entity;

import lombok.Data;

import java.time.Instant;

@Data
public class TaskDTO {
    private Long id;
    private String title;
    private String description;
    private Boolean done;
    private Byte priority;
    private Instant startTime;
}

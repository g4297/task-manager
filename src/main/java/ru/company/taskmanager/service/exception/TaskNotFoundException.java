package ru.company.taskmanager.service.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TaskNotFoundException extends RuntimeException {
    public TaskNotFoundException(Long id) {
        super("Could not find task " + id);
        log.warn("not found " + id);
    }
}

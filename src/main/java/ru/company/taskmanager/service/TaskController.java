package ru.company.taskmanager.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.company.taskmanager.data.Task;
import ru.company.taskmanager.data.TaskRepository;
import ru.company.taskmanager.service.entity.TaskDTO;
import ru.company.taskmanager.service.exception.TaskNotFoundException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@Slf4j
public class TaskController {
    private static final String PATH_TASK = "/task";
    private static final String PATH_TASK_ID = PATH_TASK + "/{id}";
    private final TaskRepository repository;

    private final JdbcTemplate jdbcTemplate;

    public TaskController(TaskRepository repository, JdbcTemplate jdbcTemplate) {
        this.repository = repository;
        this.jdbcTemplate = jdbcTemplate;
    }

    @GetMapping(PATH_TASK)
    public Iterable<Task> getAll() {
        log.info("getAll");
        Iterable<Task> found = repository.findAll();
        log.info("found " + found);
        return found;
    }

    @GetMapping(PATH_TASK_ID)
    public Task getById(@PathVariable Long id) {
        log.info("getById " + id);
        Task found = repository.findById(id).orElseThrow(() -> new TaskNotFoundException(id));
        log.info("found " + found);
        return found;
    }

    @PostMapping(PATH_TASK)
    public Task insert(@RequestBody TaskDTO newTask) {
        log.info("insert " + newTask);
        Task task = new Task();
        task.copyFromDTO(newTask);
        Task saved = repository.save(task);
        log.info("done " + saved);
        return saved;
    }

    @PutMapping(PATH_TASK_ID)
    public Task update(@RequestBody TaskDTO newTask, @PathVariable Long id) {
        log.info("update " + newTask);
        return repository.findById(id).map(task -> {
            task.copyFromDTO(newTask);
            Task saved = repository.save(task);
            log.info("done " + saved);
            return saved;
        }).orElseGet(() -> {
            Task task = new Task();
            task.copyFromDTO(newTask);
            Task saved = repository.save(task);
            log.info("done " + saved);
            return saved;
        });
    }

    @DeleteMapping(PATH_TASK_ID)
    public void delete(@PathVariable Long id) {
        log.info("delete " + id);
        repository.deleteById(id);
        log.info("done");
    }

    @GetMapping(PATH_TASK + "/findByTitle")
    public Iterable<Task> findByTitle(@RequestParam String title) {
        log.info("findByTitle " + title);
        List<Task> result = new ArrayList<>();
//        jdbcTemplate.query("select * from task where title like '%" + title + "%'",
        jdbcTemplate.query("select * from task where title like ?", new Object[]{"%" + title + "%"}, new int[] {Types.VARCHAR},
                        rs -> {
            Task t = new Task();
            t.setId(rs.getLong("id"));
            t.setTitle(rs.getString("title"));
            t.setDescription(rs.getString("description"));
            t.setDone(rs.getBoolean("done"));
            t.setPriority(rs.getByte("priority"));
            Timestamp ts = rs.getTimestamp("start_time");
            if (ts != null) {
                t.setStartTime(ts.toInstant());
            }
            result.add(t);
        });
        log.info(result.toString());
        return result;
    }

    @GetMapping("/ping")
    public String ping(@RequestParam String addr) {
        log.info("ping " + addr);
        String result = "";
        try {
            ProcessBuilder pb = new ProcessBuilder("/bin/ping", "-c", "4", addr);
            pb.redirectErrorStream(true);
            Process p = pb.start();

            List<String> results = readOutput(p.getInputStream());
            result = results.stream().collect(Collectors.joining("\r\n"));
        } catch (IOException e) {
            log.error("run process", e);
        }
        log.info(result);
        return result;
    }

    private List<String> readOutput(InputStream inputStream) throws IOException {
        try (BufferedReader output = new BufferedReader(new InputStreamReader(inputStream))) {
            return output.lines().collect(Collectors.toList());
        }
    }
}
